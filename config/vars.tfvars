# VARS
# This are the vaules of the vars that need to be defined in .tf code.

# Ami to be used. Bitnami wordpress.
my_ami = "ami-001350e7a5a5dcc7f"

# size of the vm. Tier free.
my_instance = "t2.micro"

# app name in aws
app_name = "my-single-wordpress"

# var for region.
my_region = "eu-west-1"

# public key
public_key = ""
